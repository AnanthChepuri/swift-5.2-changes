import UIKit
import SwiftUI


/*
    1.  Key Path expression as Function

    The Evolution proposal describes this as being able to use “\Root.value wherever functions of (Root) -> Value are allowed”.
 */

struct User {
    let name: String
    let age: Int
    let bestFriend: String?

    var canVote: Bool {
        age >= 18
    }
}

let eric = User(name: "Eric Effiong", age: 18, bestFriend: "Otis Milburn")
let maeve = User(name: "Maeve Wiley", age: 19, bestFriend: nil)
let otis = User(name: "Otis Milburn", age: 17, bestFriend: "Eric Effiong")
let users = [eric, maeve, otis]

let usernames = users.map { $0.name }   // Old approach
print(usernames)

let usernames1 = users.map( \.name)     // New approach
print(usernames1)

let usernames2 = users.filter( \.canVote)     // New approach
print(usernames2)

let usernames3 = users.compactMap(\.bestFriend)     // New approach
print(usernames3)

/*
    2.  Callable value as user defined nominal types ( callAsFunction() )

    A fancy way of saying that you can now call a value directly if its type implements a method named callAsFunction().
 */

struct Dice {
    var lowerBound: Int
    var upperBound: Int

    func callAsFunction() -> Int {
        (lowerBound...upperBound).randomElement()!
    }
}

let d10 = Dice(lowerBound: 1, upperBound: 6)    //  Old way
let d11 = d10.callAsFunction()
print(d11)

let d6 = Dice(lowerBound: 1, upperBound: 6)     //  New way
let d7 = Dice(lowerBound: 4, upperBound: 8)
let roll1 = d6()
print(roll1)

    //  We can use mutating for callAsFunction() also

struct StepCounter {
    var steps = 0

    mutating func callAsFunction(count: Int) -> Bool {
        steps += count
        print(steps)
        return steps > 10_000
    }
}

var steps = StepCounter()
let targetReached = steps(count: 10)
print(targetReached)

/*
    3.  Subscripts can now declare default arguments
 
    When adding custom subscripts to a type, you can now use default arguments for any of the parameters.
 */

struct PoliceForce {
    var officers: [String]

    subscript(index: Int, default default: String = "Unknown") -> String {
        if index >= 0 && index < officers.count {
            return officers[index]
        } else {
            return `default`
        }
    }
}

let force = PoliceForce(officers: ["Amy", "Jake", "Rosa", "Terry"])
print(force[0])
print(force[5])

/*
    4.  Lazy filtering order is now reversed
 
    if you use a lazy sequence such as an array, and apply multiple filters to it, those filters are now run in the reverse order.
 */

let people = ["Arya", "Cersei", "Samwell", "Stannis"]
    .lazy
    .filter { $0.hasPrefix("S") }
    .filter { print($0); return true }
let ppl = people.count
print(ppl)
print(people)

/*
   In Swift 5.2 and later that will print “Samwell” and “Stannis”, because after the first filter runs those are the only names that remain to go into the second filter. But before Swift 5.2 it would have returned all four names, because the second filter would have been run before the first one. This was confusing, because if you removed the lazy then the code would always return just Samwell and Stannis, regardless of Swift version.
*/

//  ------------

/*
    5.  New and improved diagnostics
 
    That attempts to bind a TextField view to an integer @State property, which is invalid. In Swift 5.1 this caused an error for the frame() modifier saying 'Int' is not convertible to 'CGFloat?’, but in Swift 5.2 and later this correctly identifies the error is the $name binding: Cannot convert value of type Binding<Int> to expected argument type Binding<String>.
*/

struct ContentView: View {
    @State private var name = "0"   // Declare this as Int to get error

    var body: some View {
        VStack {
            Text("What is your name?")
            TextField("Name", text: $name)
                .frame(maxWidth: 300)
        }
    }
}

